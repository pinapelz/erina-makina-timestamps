# [Softly Singing to Sleep 💤 #shorts](https://www.youtube.com/watch?v=vnxEn5uZVJQ)

## Raw Timestamps:

**Setlist/セットリスト**
- 16:56 Binks no Sake (ビンクスの酒) - Ado
- 24:09 From The Start -  Laufey
- 34:05 Rises the Moon - Liana Flores
- 42:37 This is Home - Cavetown
- 48:08 Knock Knock - Lenka
- 53:36 Fly me to the Moon
- 57:56 Can't Help Falling in Love - Elvis Presley
- 1:06:15 Cupid - FiFTY FIFTY
- 1:13:45 Drift Away (from Steven Universe)
- 1:18:44 Glassy Sky (from Tokyo Ghoul)
- 1:28:41 Summertime Record (サマータイムレコード ) from Kagerou Project (カゲプロ)
- 1:38:12 Always with Me (いつも何度でも) - Spirited Away (千と千尋の神隠し)
- 1:45:46 I see the Light from Tangled
- 1:51:48 I have a Dream - ABBA
- 2:01:58 when the party's over - Billie Eilish
- 2:13:01 Stay with Me (真夜中のドア) - Miki Matsubara (松原みき)
- 2:22:12 Don't Go/Ikanaide (いかないで)
- 2:29:27 Kainé Salvation - NieR
- 2:39:39 Loli God Requiem ロリ神レクイエム???
- 2:41:25 Weight of the World (壊レタ世界ノ歌) - NieR jp ver.
- 2:55:20 La Vie en Rose - Louis Armstrong
- 3:01:40 My Love Mine All Mine - Mitski
- 3:06:36 Promise -  Laufey

## Linked Timestamps:
- Binks no Sake (ビンクスの酒) - Ado https://youtu.be/vnxEn5uZVJQ?t=1016
- From The Start -  Laufey https://youtu.be/vnxEn5uZVJQ?t=1449
- Rises the Moon - Liana Flores https://youtu.be/vnxEn5uZVJQ?t=2045
- This is Home - Cavetown https://youtu.be/vnxEn5uZVJQ?t=2557
- Knock Knock - Lenka https://youtu.be/vnxEn5uZVJQ?t=2888
- Fly me to the Moon https://youtu.be/vnxEn5uZVJQ?t=3216
- Can't Help Falling in Love - Elvis Presley https://youtu.be/vnxEn5uZVJQ?t=3476
- Cupid - FiFTY FIFTY https://youtu.be/vnxEn5uZVJQ?t=3975
- Drift Away (from Steven Universe) https://youtu.be/vnxEn5uZVJQ?t=4425
- Glassy Sky (from Tokyo Ghoul) https://youtu.be/vnxEn5uZVJQ?t=4724
- Summertime Record (サマータイムレコード ) from Kagerou Project (カゲプロ) https://youtu.be/vnxEn5uZVJQ?t=5321
- Always with Me (いつも何度でも) - Spirited Away (千と千尋の神隠し) https://youtu.be/vnxEn5uZVJQ?t=5892
- I see the Light from Tangled https://youtu.be/vnxEn5uZVJQ?t=6346
- I have a Dream - ABBA https://youtu.be/vnxEn5uZVJQ?t=6708
- when the party's over - Billie Eilish https://youtu.be/vnxEn5uZVJQ?t=7318
- Stay with Me (真夜中のドア) - Miki Matsubara (松原みき) https://youtu.be/vnxEn5uZVJQ?t=7981
- Don't Go/Ikanaide (いかないで) https://youtu.be/vnxEn5uZVJQ?t=8532
- Kainé Salvation - NieR https://youtu.be/vnxEn5uZVJQ?t=8967
- Loli God Requiem ロリ神レクイエム??? https://youtu.be/vnxEn5uZVJQ?t=9579
- Weight of the World (壊レタ世界ノ歌) - NieR jp ver. https://youtu.be/vnxEn5uZVJQ?t=9685
- La Vie en Rose - Louis Armstrong https://youtu.be/vnxEn5uZVJQ?t=10520
- My Love Mine All Mine - Mitski https://youtu.be/vnxEn5uZVJQ?t=10900
- Promise -  Laufey https://youtu.be/vnxEn5uZVJQ?t=11196
- Once Upon a December from Anastasia https://youtu.be/vnxEn5uZVJQ?t=11758